#!/usr/bin/env python3

import argparse
import time
import pika
import sys

parser = argparse.ArgumentParser(description='Check connection to RabbitMQ server')
parser.add_argument('--server', required=True, help='Define RabbitMQ server')
parser.add_argument('--virtual_host', default='/', help='Define virtual host')
parser.add_argument('--port', type=int, default=5672, help='Define port (default: %(default)s)')
parser.add_argument('--username', default='guest', help='Define username (default: %(default)s)')
parser.add_argument('--password', default='guest', help='Define password (default: %(default)s)')
args = vars(parser.parse_args())

credentials = pika.PlainCredentials(args['username'], args['password'])
parameters = pika.ConnectionParameters(host=args['server'], port=args['port'], virtual_host=args['virtual_host'], credentials=credentials)

print('Waiting for RabbitMQ')

while True:
    try:
        connection = pika.BlockingConnection(parameters)
        if connection.is_open:
            print('RabbitMQ is up')
            connection.close()
            exit(0)
    except Exception as error:
        time.sleep(2)

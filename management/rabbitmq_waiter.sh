#!/usr/bin/env sh

usage() {
cat <<EOF
This script waits until RabbitMQ enter ready state.

EOF
exit 1
}

main() {
    if [ $# -lt 2 ]; then
        usage
    fi

    echo Waiting for RabbitMQ;

    until wget http://$@/api/aliveness-test/%2F ;
    do sleep 2;
    done;

    echo RabbitMQ is up;

    exit 0
}

main "$@"

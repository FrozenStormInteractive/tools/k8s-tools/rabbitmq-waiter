# Rabbit MQ Waiter

## Supported tags

| Version                                                                                                                          |  Status   |
|----------------------------------------------------------------------------------------------------------------------------------|-----------|
| rabbitmq-waiter:latest (registry.gitlab.com/frozenstorminteractive/tools/k8s-tools/rabbitmq-waiter:latest)                       | Supported |
| rabbitmq-waiter:latest-management (registry.gitlab.com/frozenstorminteractive/tools/k8s-tools/rabbitmq-waiter:latest-management) | Supported |

## Usage

*TODO*
